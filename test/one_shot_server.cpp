#include <thread>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "one_shot_server.hpp"

using namespace std;
using namespace caldav_test;

void OneShotServer::runner(atomic<bool> &isReady) {
  int res { };

  int sock_fd { socket(AF_UNIX, SOCK_STREAM, 0) };
  if (sock_fd < 0)
    throw runtime_error("Socket: " + string(strerror(errno)));

  sockaddr_un test_addr { AF_UNIX, "socket" };
  remove("socket");
  res = bind(
    sock_fd,
    reinterpret_cast<sockaddr *>(&test_addr),
    sizeof(test_addr));
  if (res < 0)
    throw runtime_error("Socket binding: " + string(strerror(errno)));

  if(listen(sock_fd, 0) < 0)
    throw runtime_error("Socket listening: " + string(strerror(errno)));

  isReady = true;

  int connection { accept(sock_fd, nullptr, nullptr) };
  if (connection < 0)
    throw runtime_error("Connection accepting: " + string(strerror(errno)));

  res = recv(connection, nullptr, 0, 0);

  res = send(connection, shot.c_str(), 1024, 0);
  if (res < 0)
    throw runtime_error("Sending to socket: " + string(strerror(errno)));

  close(connection);
}

future<void> OneShotServer::run_socket() {
  atomic<bool> isReady { false };
  auto socket_future { async(launch::async, runner, ref(isReady)) };

  // TODO Unpleasure. Needs refactoring.
  while (!isReady) this_thread::sleep_for(waitFor);

  return socket_future;
}

const std::string OneShotServer::RespBody { "Bee swarm" };

const map<string, string> OneShotServer::RespHeaders {
  {"date", "Mon, 01 Apr 1970 12:00:00 GMT"},
  {"server", "OneShot Server"},
  {"content-type", "text/html"},
  {"last-modified", "Mon, 01 Apr 1970 13:47:02 GMT"},
  {"content-length", to_string(RespBody.length()) },
  {"list", "this, that, other"}
};

const std::string OneShotServer::shot {
    "HTTP/1.0 200 OK\r\n"
    "Date: Mon, 01 Apr 1970 12:00:00 GMT\r\n"
    "Server: OneShot Server\r\n"
    "Content-Type: text/html\r\n"
    "List: this\r\n"
    "Last-Modified: Mon, 01 Apr 1970 13:47:02 GMT\r\n"
    "List: that\r\n"
    "Content-Length: " + to_string(RespBody.length()) + "\r\n"
    "List: other\r\n"
    "\r\n" +
    RespBody };

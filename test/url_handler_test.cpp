#define BOOST_TEST_MODULE LazyDigestTest

#include <boost/test/unit_test.hpp>
#include <iostream>
#include <string>

#include "http_response.hpp"
#include "logger.hpp"
#include "one_shot_server.hpp"
#include "url_handler.hpp"

using namespace caldav;
using namespace caldav_test;
using namespace std;

BOOST_AUTO_TEST_CASE(request) {
  UrlHandler handler { };
  handler.setUnixSocket("socket");
  handler.setTimeout(1);

  HttpResponse resp;

  auto socket_future { OneShotServer::run_socket() };

  try { resp = handler.request("localhost", queryPropPrincipal); }
  catch (UrlHandler::Error&e) { cerr << e.what() << endl; }

  socket_future.get();

  BOOST_CHECK_EQUAL(resp.getBody(), OneShotServer::RespBody);
  BOOST_TEST(resp.getHeaders() == OneShotServer::RespHeaders);
}

BOOST_AUTO_TEST_CASE(counter) {
  BOOST_CHECK_EQUAL(UrlHandler::getInstancesCounter(), 0);
  UrlHandler handler0 { };
  BOOST_CHECK_EQUAL(UrlHandler::getInstancesCounter(), 1);
  auto handler_ptr0 = new UrlHandler{ };
  auto handler_ptr1 = new UrlHandler{ };
  auto handler_ptr2 = new UrlHandler{ };
  BOOST_CHECK_EQUAL(UrlHandler::getInstancesCounter(), 4);
  delete handler_ptr0;
  BOOST_CHECK_EQUAL(UrlHandler::getInstancesCounter(), 3);
  UrlHandler handler1 { *handler_ptr1 };
  BOOST_CHECK_EQUAL(UrlHandler::getInstancesCounter(), 4);
  UrlHandler handler2 { move(*handler_ptr1) };
  BOOST_CHECK_EQUAL(UrlHandler::getInstancesCounter(), 5);
  delete handler_ptr1;
  BOOST_CHECK_EQUAL(UrlHandler::getInstancesCounter(), 4);
  delete handler_ptr2;
  BOOST_CHECK_EQUAL(UrlHandler::getInstancesCounter(), 3);
}

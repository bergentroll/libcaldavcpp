#define BOOST_TEST_MODULE CalDavClientTest

#include <boost/test/unit_test.hpp>

#include "caldav_client.hpp"

using namespace std;
using namespace caldav;

BOOST_AUTO_TEST_SUITE(getRootUrlTestSuite);

BOOST_AUTO_TEST_CASE(ascii_http) {
  auto input { "http://example.com/some_path?var=value" };
  auto expected { "http://example.com" };
  BOOST_CHECK_EQUAL(getRootUrl(input), expected);
}

BOOST_AUTO_TEST_CASE(ascii_https) {
  auto input { "https://example.com/some_path?var=value" };
  auto expected { "https://example.com" };
  BOOST_CHECK_EQUAL(getRootUrl(input), expected);
}

BOOST_AUTO_TEST_CASE(from_cyrillic) {
  auto input { "https://пример.рф/некий_путь?пер=значение" };
  auto expected { "https://пример.рф" };
  BOOST_CHECK_EQUAL(getRootUrl(input), expected);
}

BOOST_AUTO_TEST_CASE(from_punycoded) {
  auto input {
    "https://xn--e1afmkfd.xn--p1ai/%D0%BD%D0%B5%D0%BA%D0%B8%D0%B9_%"
    "D0%BF%D1%83%D1%82%D1%8C?%D0%BF%D0%B5%D1%80=%D0%B7%D0%BD%D0%B0%D1%87%D0%B5%D"
    "0%BD%D0%B8%D0%B5"
  };
  auto expected { "https://xn--e1afmkfd.xn--p1ai" };
  BOOST_CHECK_EQUAL(getRootUrl(input), expected);
}

BOOST_AUTO_TEST_SUITE_END();


BOOST_AUTO_TEST_SUITE(splitItemUrlTestSuite);

BOOST_AUTO_TEST_CASE(split_item_href) {
  auto result { splitItemUrl("/super%20cal/my+item.ics") };
  BOOST_CHECK_EQUAL(result.first, "/super%20cal/");
  BOOST_CHECK_EQUAL(result.second, "my+item.ics");
}

BOOST_AUTO_TEST_CASE(split_redundant_item_href) {
  auto result { splitItemUrl("//super%20cal//my+item.ics") };
  BOOST_CHECK_EQUAL(result.first, "//super%20cal//");
  BOOST_CHECK_EQUAL(result.second, "my+item.ics");
}

BOOST_AUTO_TEST_SUITE_END();

#define BOOST_TEST_MODULE CalDavHandlerTest

#include <boost/test/unit_test.hpp>
#include <chrono>
#include <memory>
#include <string>

#include "caldav_handler.hpp"
#include "http_response.hpp"
#include "logger.hpp"
#include "url_handler.hpp"

using namespace std;

string task() {
  static int uid { };
  return
    "BEGIN:VCALENDAR\n"
    "VERSION:2.0\n"
    "BEGIN:VTODO\n"
    "DTSTAMP:20190301T125741\n"
    "UID:" + to_string(uid++) + "\n"
    "CREATED:20190228T102313Z\n"
    "LAST-MODIFIED:20190301T125741\n"
    "SUMMARY:To do important thing!\n"
    "STATUS:COMPLETED\n"
    "PERCENT-COMPLETE:100\n"
    "END:VTODO\n"
    "END:VCALENDAR\n";
};

string make_username() {
  return
    string(boost::unit_test::framework::current_test_case().p_name) +
    to_string(chrono::system_clock::to_time_t(chrono::system_clock::now()));
}

bool contains(string const & str, string const & substr) {
  return (str.find(substr) != string::npos);
}

template <typename F>
void TEST_BAD_STATUSCODE(F func, int code) {
  try {
    func();
    BOOST_ERROR("An exception has been expected.");
  }
  catch (caldav::HttpResponse::UnsuccessfulStatus const & e) {
    BOOST_CHECK_EQUAL(e.getStatusCode(), code);
  }
}

BOOST_AUTO_TEST_CASE(getCalendarHomeSet) {
  const string username { make_username() };

  auto urlHandler { make_shared<caldav::UrlHandler>(username, "password") };
  caldav::CalDavHandler caldav { urlHandler };

  BOOST_TEST(caldav.getPrincipalPath("http://localhost:5232"));
  BOOST_TEST(caldav.getCalendarHomeSet("http://localhost:5232/" + username + "/"));
  BOOST_CHECK_THROW(caldav.getPrincipalPath("http://0.0.0.0"), caldav::UrlHandler::Error);
}

BOOST_AUTO_TEST_CASE(createCalendars) {
  const string username { make_username() };

  auto urlHandler { make_shared<caldav::UrlHandler>(username, "password") };
  caldav::CalDavHandler caldav { urlHandler };

  BOOST_TEST(caldav.listHomeSet("http://localhost:5232/" + username + "/"));

  BOOST_TEST(caldav.appendCalendar("http://localhost:5232/" + username + "/book1"));
  BOOST_TEST(caldav.appendCalendar("http://localhost:5232/" + username + "/book2"));

  TEST_BAD_STATUSCODE(
    [&]() { caldav.appendCalendar("http://localhost:5232/" + username + "/book1"); },
    409);

  BOOST_TEST(
      contains(caldav.listHomeSet("http://localhost:5232/" + username + "/").getBody(),
      "book1"));
  BOOST_TEST(
      contains(caldav.listHomeSet("http://localhost:5232/" + username + "/").getBody(),
      "book2"));
}

BOOST_AUTO_TEST_CASE(createItems) {
  const string username { make_username() };

  auto urlHandler { make_shared<caldav::UrlHandler>(username, "password") };
  caldav::CalDavHandler caldav { urlHandler };

  BOOST_TEST(caldav.appendCalendar("http://localhost:5232/" + username + "/book1"));
  BOOST_TEST(caldav.appendItem("http://localhost:5232/" + username + "/book1/task1.ics", task()));
  BOOST_TEST(caldav.appendItem("http://localhost:5232/" + username + "/book1/task2.ics", task()));
  BOOST_TEST(caldav.appendItem("http://localhost:5232/" + username + "/book1/task3.ics", task()));
  TEST_BAD_STATUSCODE(
    [&]() { caldav.appendItem("http://localhost:5232/" + username + "/book2/task1.ics", task()); },
    409);

  BOOST_TEST(caldav.appendCalendar("http://localhost:5232/" + username + "/book2"));
  BOOST_TEST(caldav.appendItem("http://localhost:5232/" + username + "/book2/task1.ics", task()));

  auto responseItems { caldav.getItems("http://localhost:5232/" + username + "/book2/task1.ics") };
  BOOST_TEST(contains(responseItems.getBody(), "task1"));
  BOOST_TEST(contains(responseItems.getBody(), "important thing"));
}

BOOST_AUTO_TEST_CASE(getCTag) {
  const string username { make_username() };

  auto urlHandler { make_shared<caldav::UrlHandler>(username, "password") };
  caldav::CalDavHandler caldav { urlHandler };

  BOOST_TEST(caldav.appendCalendar("http://localhost:5232/" + username + "/book1"));

  auto response = caldav.getCTag("http://localhost:5232/" + username + "/book1");
  BOOST_TEST(response);

  BOOST_TEST(caldav.appendItem("http://localhost:5232/" + username + "/book1/task1.ics", task()));
  BOOST_CHECK_NE(response.getBody(), caldav.getCTag("http://localhost:5232/" + username + "/book1").getBody());
}

BOOST_AUTO_TEST_CASE(removeItem) {
  const string username { make_username() };

  auto urlHandler { make_shared<caldav::UrlHandler>(username, "password") };
  caldav::CalDavHandler caldav { urlHandler };

  BOOST_TEST(caldav.appendCalendar("http://localhost:5232/" + username + "/book1"));
  BOOST_TEST(caldav.appendItem("http://localhost:5232/" + username + "/book1/task1.ics", task()));
  BOOST_TEST(caldav.appendItem("http://localhost:5232/" + username + "/book1/task2.ics", task()));
  BOOST_TEST(caldav.appendItem("http://localhost:5232/" + username + "/book1/task3.ics", task()));
  BOOST_TEST(caldav.remove("http://localhost:5232/" + username + "/book1/task3.ics"));
  BOOST_TEST(caldav.remove("http://localhost:5232/" + username + "/book1"));

  BOOST_TEST_CHECKPOINT("After removing");

  TEST_BAD_STATUSCODE(
    [&]() { caldav.getItems("http://localhost:5232/" + username + "/book1"); },
    404);
}

BOOST_AUTO_TEST_CASE(renameCalendar) {
  const string username { make_username() };

  auto urlHandler { make_shared<caldav::UrlHandler>(username, "password") };
  caldav::CalDavHandler caldav { urlHandler };

  BOOST_TEST(caldav.appendCalendar("http://localhost:5232/" + username + "/book1"));
  BOOST_TEST(caldav.appendItem("http://localhost:5232/" + username + "/book1/task1.ics", task()));
  BOOST_TEST(caldav.appendItem("http://localhost:5232/" + username + "/book1/task2.ics", task()));
  BOOST_TEST(caldav.appendItem("http://localhost:5232/" + username + "/book1/task3.ics", task()));
  BOOST_TEST(caldav.renameCalendar("http://localhost:5232/" + username + "/book1", "BOOK ONE"));

  auto responseItems { caldav.listHomeSet("http://localhost:5232/" + username + "/") };
  BOOST_TEST(responseItems);
  BOOST_TEST(contains(responseItems.getBody(), "BOOK ONE"));
}

BOOST_AUTO_TEST_CASE(updateItem) {
  const string username { make_username() };

  auto urlHandler { make_shared<caldav::UrlHandler>(username, "password") };
  caldav::CalDavHandler caldav { urlHandler };

  BOOST_TEST(caldav.appendCalendar("http://localhost:5232/" + username + "/book1"));

  auto taskStr { task() };

  BOOST_TEST(caldav.appendItem("http://localhost:5232/" + username + "/book1/task1.ics", taskStr));

  auto response1 { caldav.getItem("http://localhost:5232/" + username + "/book1/task1.ics") };

  string subStr { "COMPLETED" };
  taskStr.replace(taskStr.find(subStr), subStr.length(), "IN-PROCESS");
  BOOST_TEST(caldav.updateItem("http://localhost:5232/" + username + "/book1/task1.ics", taskStr));

  auto response2 { caldav.getItem("http://localhost:5232/" + username + "/book1/task1.ics") };

  BOOST_CHECK_NE(response1.getHeaders().at("etag"), response2.getHeaders().at("etag"));
}

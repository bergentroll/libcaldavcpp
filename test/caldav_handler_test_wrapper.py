#!/usr/bin/env python3

import contextlib
import shutil
import subprocess
import sys
import time

import requests

if __name__ == '__main__':
    STORAGE_DIR = 'radicale-storage'
    URL = 'http://localhost:5232'
    LOGFILE_NAME = 'radicale.log'

    with contextlib.suppress(requests.exceptions.ConnectionError):
        requests.get(URL)
        raise SystemError('socket is binded already')

    with open(LOGFILE_NAME, 'a') as radicale_log:
        radicale_proc = subprocess.Popen(
            ['python3', '-m', 'radicale',
             f'--storage-filesystem-folder={STORAGE_DIR}',
             '--logging-level=debug'], stderr=radicale_log)

        TIMEOUT = 5.0
        STEP = 0.2

        # Wait until Radical server is starting.
        while TIMEOUT > 0:
            RADICALE_RETCODE = radicale_proc.poll()
            if RADICALE_RETCODE is not None:
                raise SystemError(
                    f'radicale server exit with code {RADICALE_RETCODE}, '
                    f'see {LOGFILE_NAME}')
            with contextlib.suppress(requests.exceptions.ConnectionError):
                requests.get('http://localhost:5232')
                break
            TIMEOUT = TIMEOUT - STEP
            time.sleep(STEP)

        if TIMEOUT <= 0:
            raise SystemError('radicale server does not response')

        test_process = subprocess.run(
            ['./caldav_handler_test'] + sys.argv[1:],
            check=False)

        shutil.rmtree(STORAGE_DIR)
        radicale_proc.kill()

        sys.exit(test_process.returncode)

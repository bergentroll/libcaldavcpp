#pragma once

#include <atomic>
#include <map>
#include <string>
#include <future>

namespace caldav_test {
  class OneShotServer {
  public:
    static const std::map<std::string, std::string> RespHeaders;
    static const std::string RespBody;

    [[ nodiscard ]]
    static std::future<void> run_socket();
  private:
    static const std::string shot;
    static const inline auto waitFor { std::chrono::milliseconds(10) };

    static void runner(std::atomic<bool> &isReady);
  };
}

#include <algorithm>
#include <iostream>
#include <utility>

#include "url_handler.hpp"
#include "logger.hpp"

using namespace caldav;
using namespace std;


UrlHandler::UrlHandler(string username, string password) {
  if (counter == 0) {
    curl_global_init(CURL_GLOBAL_DEFAULT);
  }

  curl = curl_easy_init();

  if (!curl) {
    if (counter == 0) curl_global_cleanup();
    throw Error("Failed to initialize curl instance");
  }

  counter += 1;

  curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, headerCallback);
  setUserAgent(defaultUserAgent);
  setCredentials(username, password);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeCallback);
  curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, headerCallback);
}

UrlHandler::~UrlHandler() {
  if (counter == 0) {
    INFO
      << "Warning: The counter of UrlHandler instances is drained while at "
      << "least one instance still exists.";
  }
  curl_easy_cleanup(curl);
  counter -= 1;
  if (counter == 0) {
    curl_global_cleanup();
  }
}

UrlHandler::UrlHandler(const UrlHandler &other) {
  curl = curl_easy_duphandle(other.curl);
  if (!curl) { throw Error("Failed to duplicate curl instance"); }
  counter += 1;
}

UrlHandler& UrlHandler::operator=(const UrlHandler &other) {
  curl_easy_cleanup(curl);
  curl = curl_easy_duphandle(other.curl);
  if (!curl) { throw UrlHandler::Error("Failed to duplicate curl instance"); }
  return *this;
};

UrlHandler::UrlHandler(UrlHandler &&other): curl{other.curl} {
  other.curl = nullptr;
  counter += 1;
}

UrlHandler& UrlHandler::operator=(UrlHandler &&other) {
  if (&other != this) {
    curl_easy_cleanup(curl);
    curl = other.curl;
    other.curl = nullptr;
  }
  return *this;
}

void UrlHandler::setCredentials(string username, string password) {
  curl_easy_setopt(curl, CURLOPT_USERNAME, username.c_str());
  curl_easy_setopt(curl, CURLOPT_PASSWORD, password.c_str());
}

size_t UrlHandler::writeCallback(void *content, size_t size, size_t nmemb, string *buffer) {
  buffer->append(static_cast<char *>(content), size * nmemb);
  return size * nmemb;
}

size_t UrlHandler::headerCallback(
    char *content, size_t size, size_t nitems, map<string, string> *buffer) {
  string::size_type delimPos, delimSize;
  string strBuf, hName, hValue;
  string delim = ": ";
  delimSize = delim.size();

  DEBUG << "RAW HEADER: " << content;

  strBuf.append(static_cast<char *>(content), size * nitems);

  // Attribution to https://stackoverflow.com/a/217605/7486328.
  strBuf.erase(
    find_if(
      strBuf.rbegin(),
      strBuf.rend(),
      [](char ch) { return !isspace(ch); }).base(),
    strBuf.end());

  delimPos = strBuf.find(delim);
  if (delimPos != string::npos) {
    hName = strBuf.substr(0, delimPos);
    transform(hName.begin(), hName.end(), hName.begin(), ::tolower);
    hValue= strBuf.substr(delimPos + delimSize);
  }

  if (!hName.empty()) {
    if (buffer->count(hName)) (*buffer)[hName] += ", " + hValue;
    else (*buffer)[hName] = hValue;
  }

  return size * nitems;
}

HttpResponse UrlHandler::request(string const & url, Query const & query) {
  long responseCode { };
  HttpResponse::HeadersType responseHeaders { };
  std::string responseBody { };

  struct curl_slist *headers = nullptr;
  for (string header: query.headers)
    headers = curl_slist_append(headers, header.c_str());
  headers = curl_slist_append(headers, "Prefer: return-minimal");

  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
  curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, query.method.c_str());
  curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, query.data.c_str());
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &responseBody);
  curl_easy_setopt(curl, CURLOPT_HEADERDATA, &responseHeaders);

  CURLcode result;
  result = curl_easy_perform(curl);

  curl_slist_free_all(headers);

  if(result == CURLE_OK) {
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &responseCode);
  }
  else {
    switch (result) {
      case CURLE_URL_MALFORMAT:
        throw Error("URL \"" + url +  "\" is malformed");
        break;
      default:
        throw Error("cURL said: " + string(curl_easy_strerror(result)));
        break;
    }
  }

  return {
    static_cast<int>(responseCode),
    move(responseHeaders),
    move(responseBody)
  };
}

#include <sstream>
#include <regex>

#include "caldav_client.hpp"
#include "logger.hpp"
#include "xml_parser.hpp"

using namespace caldav;
using namespace std;

CalDavClient::CalDavClient(shared_ptr<Requestable> urlHandler, string baseUrl):
handler(urlHandler), rootUrl(getRootUrl(baseUrl)) {
  auto principal_resp { handler.getPrincipalPath(baseUrl) };
  auto principalPath {
    XmlParser(principal_resp.getBody()).get(
        "//*[local-name()='current-user-principal']/*[local-name()='href']")
  };

  INFO << "Got principal path: " << principalPath << endl;

  auto cal_home_resp { handler.getCalendarHomeSet(rootUrl + principalPath) };
  this->calendarHomeSet = XmlParser(cal_home_resp.getBody()).get(
    "//*[local-name()='calendar-home-set']/*[local-name()='href']");

  INFO << "Got calendar home set: " << calendarHomeSet << endl;
}

void CalDavClient::sync() {
  auto resp { handler.listHomeSet(rootUrl + calendarHomeSet) };
  XmlParser xmlParser { resp.getBody() };

  cout << string(xmlParser) << endl; // TODO

  // TODO
  string xPath_resp { "//*[local-name()='response' and descendant::*[local-name()='resourcetype' and child::*[local-name()='calendar']]]"};

  vector<string> calUrls {
    xmlParser.getMultiple(xPath_resp + "/*[local-name()='href']")
  };

  vector<string> calDisplayNames {
    xmlParser.getMultiple(xPath_resp + "//*[local-name()='displayname']")
  };

  vector<string> calCTags {
    xmlParser.getMultiple(xPath_resp + "//*[local-name()='getctag']")
  };

  // TODO Custom exception type
  if (calUrls.size() != calDisplayNames.size() || calUrls.size() != calCTags.size())
    throw logic_error("Unmatched amount of found calendars' hrefs, displaynames and ctags");

  for (size_t i = 0; i < calUrls.size(); ++i) {
    data[calUrls[i]] = { Node::State::NORMAL, Node::Type::COLLECTION, calCTags[i], "" };
    // TODO Displaynames
  }
}

CalDavClient::operator string() const {
  stringstream ss { };
  ss
    << "rootUrl: " << rootUrl << '\n'
    << "caldendarHomeSet: " << calendarHomeSet << '\n';

  for (auto const & item_pair: data) {
    ss
      << item_pair.first << '\n'
      << string(item_pair.second) << endl;
  }

  return ss.str();
}

namespace caldav {
  string getRootUrl(string url) {
    regex regex("((^http.?://|^)(.+?))(/|$)");
    smatch match;
    regex_search(url, match, regex);
    return match[1];
  }

  pair<string, string> splitItemUrl(string itemUrl) {
    pair <string, string> result;
    unsigned int pos = itemUrl.rfind('/') + 1;
    result.first = itemUrl.substr(0, pos);
    result.second = itemUrl.substr(pos, itemUrl.length() - pos);
    return result;
  }
}

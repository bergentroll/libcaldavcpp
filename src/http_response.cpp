#include <ostream>

#include "http_response.hpp"

using namespace std;
using namespace caldav;

map<int, string> HttpResponse::HttpStatusCodes = {
  {  -1, "Request did not performed" },
  {   0, "No response" },
  { 200, "Ok" },
  { 400, "Bad Request" },
  { 401, "Unauthorized" },
  { 403, "Forbidden" },
  { 404, "Not Found" },
  { 405, "Method Not Allowed" },
  { 407, "Proxy Authentication Request" },
  { 408, "Request Timeout" },
  { 413, "Payload Too Large" },
  { 414, "Uri Too Long" },
  { 418, "Im A Teapot" },
  { 419, "Authentication Timeout" },
  { 429, "Too Many Requests" },
  { 500, "Internal Server Error" },
  { 501, "Not Implemented" },
  { 502, "Bad Gateway" },
  { 503, "Service Unavailable" },
  { 504, "Gateway Timeout" },
  { 505, "Http Version Not Supported" },
  { 507, "Insufficient Storage" },
  { 508, "Loop Detected" },
  { 530, "Unknown Error" },
  { 521, "Web Server Is Down" },
  { 522, "Connection Timed Out" },
  { 525, "SSL Handshake Failed" },
  { 526, "Invalid Ssl Certificate" }
};

string HttpResponse::getStatusString() const {
  string s = "HTTP status code " + to_string(statusCode);
  if (auto match = HttpStatusCodes.find(statusCode); match != HttpStatusCodes.end())
    s += ": " + match->second;
  return s;
}

ostream & caldav::operator<<(ostream & stream, HttpResponse const & response) {
  stream << response.getStatusString() << endl;
  for (auto const & header: response.getHeaders()) {
    stream << header.first << ": " << header.second << endl;
  }
  stream << response.getBody() << endl;
  return stream;
}

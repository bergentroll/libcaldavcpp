#include "query.hpp"

namespace caldav {
  QueryPropfind::QueryPropfind(std::string const & field):
  Query({"", "PROPFIND", { "Depth: 0" }}) {
    data =
      "<d:propfind "
            "xmlns:c='urn:ietf:params:xml:ns:caldav' "
            "xmlns:cs='http://calendarserver.org/ns/' "
            "xmlns:d='DAV:'>"
        "<d:prop>"
          + field +
        "</d:prop>"
      " </d:propfind>";
  }

  QueryProppatchDisplayname::QueryProppatchDisplayname(std::string const & displayname):
  Query({ "", "PROPPATCH", { } }) {
    data =
      "<d:propertyupdate xmlns:d='DAV:'>"
        "<d:set>"
          "<d:prop>"
            "<d:displayname>" + displayname + "</d:displayname>"
          "</d:prop>"
        "</d:set>"
      "</d:propertyupdate>";
  }

  QueryMkCalendar::QueryMkCalendar(std::string const & displayname):
  Query({ "", "MKCALENDAR", { } }) {
    data =
      "<c:mkcalendar xmlns:d='DAV:' xmlns:c='urn:ietf:params:xml:ns:caldav'>"
        "<d:set>"
          "<d:prop>"
            "<d:displayname>" + displayname + "</d:displayname>"
          "</d:prop>"
        "</d:set>"
      "</c:mkcalendar>";
  }

  const Query queryCalendars {
    "<d:propfind "
          "xmlns:c='urn:ietf:params:xml:ns:caldav' "
          "xmlns:cs='http://calendarserver.org/ns/' "
          "xmlns:d='DAV:'>"
      "<d:prop>"
        "<d:resourcetype />"
        "<d:displayname />"
        "<cs:getctag />"
        "<c:supported-calendar-component-set />"
      "</d:prop>"
    "</d:propfind>",

    "PROPFIND",

    { "Depth: 1" }
  };

  /// @todo Parametrize with filter.
  const Query queryDownload {
    "<c:calendar-query xmlns:c='urn:ietf:params:xml:ns:caldav' xmlns:d='DAV:'>"
      "<d:prop>"
        "<d:getetag />"
        "<c:calendar-data />"
      "</d:prop>"
      "<c:filter>"
        "<c:comp-filter name='VCALENDAR'>"
          //"<c:comp-filter name='VTODO' />"
        "</c:comp-filter>"
      "</c:filter>"
    "</c:calendar-query>",

    "REPORT",

    { "Depth: 1" }
  };

  const Query queryGet { "", "GET" };

  const Query queryPut {
    "",

    "PUT",

    { "Content-Type: text/calendar;" }
  };

  const Query queryDelete { "", "DELETE" };
}

#include "xml_parser.hpp"

#include <sstream>

using namespace std;
using namespace caldav;

XmlParser::XmlParser(string const & xml) {
  pugi::xml_parse_result parse_result = xmlDoc.load_string(xml.c_str());
  if (parse_result.status != pugi::status_ok) {
    throw runtime_error(
      "XML parsing error: " + string(parse_result.description()) + ", data was: " + xml);
  }
}

// TODO Speedup with select_node()
string XmlParser::get(string const & xPath) {
  auto result { getMultiple(xPath) };

  if (result.size() != 1) {
    stringstream ss { };
    ss
      << "Expected exactly 1 match for query, but " << to_string(result.size()) << " obtained, "
      << "query was: " << xPath << ", data was: " << string(*this);
    throw logic_error(ss.str());
  }

  return result[0];
}

vector<string> XmlParser::getMultiple(string const & xPath) {
  vector<string> result { };

  pugi::xpath_node_set entries = xmlDoc.select_nodes(xPath.c_str());
  entries.sort();

  for (auto it: entries) result.push_back(it.node().text().get());

  return result;
}

XmlParser::operator string() {
  stringstream ss { };
  xmlDoc.save(ss, "  ");
  return ss.str();
}

#include "node.hpp"

#include <sstream>

using namespace std;
using namespace caldav;

map<Node::Type, string> Node::typeDic {
  { Type::COLLECTION, "COLLECTION" },
  { Type::ITEM, "ITEM" },
};

map<Node::State, string> Node::stateDic {
  { State::NEW, "NEW" },
  { State::NORMAL, "NORMAL" },
  { State::DELETED, "DELETED" },
};

Node::operator std::string() const {
  stringstream ss { };

  ss
    << "state: " << stateDic[state] << '\n'
    << "type: " << typeDic[type] << '\n'
    << "tag: " << tag << '\n'
    << "data: " << data;

  return ss.str();
}

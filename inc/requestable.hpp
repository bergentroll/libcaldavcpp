#pragma once

#include <string>

#include "http_response.hpp"
#include "query.hpp"

namespace caldav {
  /** @brief Interface for UrlHandler.
   *
   *  Mostly needed for testing purpose.
   */
  class Requestable {
  public:
    virtual ~Requestable() { };

    /** @brief Performs request.
     *  @return Server response.
     */
    virtual HttpResponse request(std::string const & url, const Query &query) = 0;

    /**
     */
    HttpResponse getOrThrow(std::string const & url, const Query &query) {
      auto result { request(url, query) };
      result.throwIfUnsuccessful();
      return result;
    }
  };
}

﻿#pragma once

#include <string>
#include <map>

namespace caldav {
  class Node {
  public:
    enum class Type { COLLECTION, ITEM };
    enum class State { NEW, NORMAL, DELETED };

    State state { State::NORMAL };
    Type type;
    std::string tag { };
    std::string data { };

    operator std::string() const;

  private:
    static std::map<Type, std::string> typeDic;
    static std::map<State, std::string> stateDic;
  };
}

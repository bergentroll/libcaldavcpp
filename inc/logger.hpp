#pragma once

#include <iostream>

namespace caldav {
  enum class LogLevel { DEBUG, INFO, ERROR };

  /// Journaling output.
  class Logger {
    public:
      /// Type alias for stream manipulators.
      using MANIP = std::ostream& (*)(std::ostream &);

      /// Application level logging verbosity.
      static inline LogLevel logLevel { LogLevel::ERROR };

      explicit Logger(LogLevel level): thisLogLevel(level) { }

      /** @brief Fire output.
       *  @param input Anything std::string convertable.
       *
       *  Usage:
       *  @code MY_LOGGER("Information"); @endcode
       */
      template <typename T>
      void operator()(T && input) const {
        if (thisLogLevel >= logLevel) stream << input;
      }

      /** @brief Fire output.
       *
       *  @param input Anything std::string convertable.
       *  @return This logger to path next item.
       *
       *  Usage:
       *  @code MY_LOGGER << "Value: " << var; @endcode
       */
      template <typename T>
      Logger const & operator<<(T && input) const {
        (*this)(input);
        return *this;
      }

      /** @brief Use ostream manipulators.
       *
       *  @param manip Some ostream manipulator, e.g. std::endl.
       *  @return This logger to path next item.
       */
      Logger const & operator<<(MANIP manip) const {
        manip(stream);
        return *this;
      }

    private:
      static inline std::ostream & stream { std::cerr };
      LogLevel thisLogLevel;
  };

  /// Global instance for debug logging.
  Logger const DEBUG { LogLevel::DEBUG };

  /// Global instance for info logging.
  Logger const INFO { LogLevel::INFO };

  /// Global instance for error logging.
  Logger const ERROR { LogLevel::ERROR };
}

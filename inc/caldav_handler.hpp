#pragma once

#include <string>
#include <memory>

#include "http_response.hpp"
#include "url_handler.hpp"

namespace caldav {
  /** @brief Provide methods to request CalDAV server.
   *
   *  Glue between Query and UrlHandler.
   */
  class CalDavHandler {
  public:
    /** @param urlHandler Supposed to be a configured UrlHandler instance.
     */
    explicit CalDavHandler(std::shared_ptr<Requestable> urlHandler) noexcept:
    urlHandler(urlHandler) { }

    /** @brief Request for relative path of user page.
     *
     *  Principal path useful to request path to user's calendars with
     *  getPrincipalPath.
     *  @param baseUrl URL of CalDAV server entry point. See documentation of
     *  your server.
     *  @return Server response.
     *  @throw UrlHandler::Error On connection troubles.
     */
    HttpResponse getPrincipalPath(std::string const & baseUrl) const {
      return { urlHandler->getOrThrow(baseUrl, queryPropPrincipal) };
    }

    /** @brief Request for calendar home relative path.
     *
     *  Calendar home path provides ability to get list of calendars with
     *  listHomeSet method.
     *  @param principalUrl Link to CalDAV user, which may be obtained with
     *  getPrincipalPath.
     *  @return Server response.
     *  @throw UrlHandler::Error On connection troubles.
     */
    HttpResponse getCalendarHomeSet(std::string const & principalUrl) const {
      return { urlHandler->getOrThrow(principalUrl, queryPropCalendarHomeSet) };
    }

    /** @brief Get list of caldendars.
     *
     *  Calendar is collection of objects. User may own multiple calendars.
     *  Method requests for all caldendars of user including resourcetype,
     *  displayname and ctag.
     *  @param calendarHomeSetUrl Link to collections of user, wich may be
     *  obtained with getCalendarHomeSet.
     *  @return Server response.
     *  @throw UrlHandler::Error On connection troubles.
     */
    HttpResponse listHomeSet(std::string const & calendarHomeSetUrl) const {
      return { urlHandler->getOrThrow(calendarHomeSetUrl, queryCalendars) };
    }

    /** @brief Download full calendar content.
     *
     *  Request usually gives a Multi-Status response which contains all
     *  calendar objects.
     *  @param calendarUrl Link to calendar.
     *  @return Server response.
     *  @throw UrlHandler::Error On connection troubles.
     */
    HttpResponse getItems(std::string const & calendarUrl) const {
      return { urlHandler->getOrThrow(calendarUrl, queryDownload) };
    }

    /** Download specific calendar object.
     *
     *  Method may be useful to get ETag after item creation or update. ETag
     *  looks like ctag: changes means that object has been updated.
     *  @param itemUrl Link to calendar object.
     *  @return Server response.
     *  @throw UrlHandler::Error On connection troubles.
     */
    HttpResponse getItem(std::string const & itemUrl) const {
      return { urlHandler->getOrThrow(itemUrl, queryGet) };
    }

    /** @brief Get calendar ctag.
     *
     *  ctag is a calendar property, that indicates changes. It is like hash,
     *  but it may be just a random string.
     *  @param calendarUrl Link to calendar.
     *  @return Server response.
     *  @throw UrlHandler::Error On connection troubles.
     */
    HttpResponse getCTag(std::string const & calendarUrl) const {
      return { urlHandler->getOrThrow(calendarUrl, queryGetCTag) };
    }

    /** @brief Add calendar object to server.
     *
     *  @param itemUrl Link to calendar object.
     *  @param data Valid iCalendar data. UID must be unique if provided.
     *  @return Server response.
     *  @throw UrlHandler::Error On connection troubles.
     */
    HttpResponse appendItem(std::string const & itemUrl, std::string const & data) const {
      return updateItem(itemUrl, data);
    }

    /** @brief Change existed calendar object.
     *
     *  @param itemUrl Link to calendar object.
     *  @param data Valid iCalendar data. UID must be unique if provided.
     *  @return ETag of item.
     *  @throw UrlHandlerError
     *  @throw std::out_of_range
     */
    HttpResponse updateItem(std::string const & itemUrl, std::string const & data) const {
      auto query { queryPut };
      query.data = data;
      return { urlHandler->getOrThrow(itemUrl, query) };
    }

    /** @brief Create new calendar.
     *
     *  @param calendarUrl Link to calendar.
     *  @param displayname User-friendly name for collection. Is optional.
     *  Usually is provided by user.
     *  @return Server response.
     *  @throw UrlHandler::Error On connection troubles.
     */
    HttpResponse appendCalendar(std::string const & calendarUrl, std::string const & displayname="") const {
      return { urlHandler->getOrThrow(calendarUrl, QueryMkCalendar(displayname)) };
    }

    /** @brief Change calendar displayname.
     *
     *  @param calendarUrl Link to calendar.
     *  @param displayname New name.
     *  @return Server response.
     *  @throw UrlHandler::Error On connection troubles.
     */
    HttpResponse renameCalendar(std::string const & calendarUrl, std::string const & displayname) const {
      return { urlHandler->getOrThrow(calendarUrl, QueryProppatchDisplayname(displayname)) };
    }

    /** @brief Remove calendar or calendar object.
     *
     *  Removes calendar with all objects. Usually action can not be undone.
     *  @param url Link to calendar or calendar object.
     *  @return Server response.
     *  @throw UrlHandler::Error On connection troubles.
     */
    HttpResponse remove(std::string const & url) const {
      return urlHandler->getOrThrow(url, queryDelete);
    }

  private:
    std::shared_ptr<Requestable> urlHandler;
  };
}

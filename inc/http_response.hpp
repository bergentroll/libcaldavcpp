#pragma once

#include <map>
#include <stdexcept>
#include <string>

namespace caldav {
  /// HTTP response data.
  class HttpResponse {
  public:
    /// Responce status is not successful.
    class UnsuccessfulStatus: public std::runtime_error {
    public:
      explicit UnsuccessfulStatus(std::string const & errorMessage, int statusCode=-1):
      std::runtime_error(errorMessage), statusCode(statusCode) { }

      int getStatusCode() const { return statusCode; }

    private:
      int statusCode;
    };

    /// Alias for headers carrying type.
    using HeadersType = std::map<std::string, std::string>;

    HttpResponse() = default;

    HttpResponse(
      int statusCode,
      HeadersType const & headers,
      std::string const & body):
    statusCode(statusCode), headers(headers), body(body) { }

    /** @brief Get HTTP numeric status code.
     *
     * @return Standard HTTP status code:\n
     *  0 — when no response code recieved,\n
     *  200-299 — success,\n
     *  300-399 — redirections,\n
     *  400-499 — client errors,\n
     *  500-599 — server errors.
     */
    int getStatusCode() { return statusCode; }

    /// Get body of response message.
    std::string getBody() const { return body; }

    /** @brief Response headers.
     *
     * UrlHandler saves headers in form { name, value }, where name is
     * guranteed to be in lower-case.
     * @return Container of headers.
     */
    HeadersType getHeaders() const { return headers; }

    /** @brief Get short status message.
     *
     *  Could be useful for errors reporting.
     *  @return Formatted status string.
     */
    std::string getStatusString() const;

    /** @brief Convertion to bool. True when request was performed and succeed.
     *
     * @return true if response seems successful, false otherwise.
     */
    operator bool() const { return (statusCode >= 200 && statusCode < 300); }

    /**
     */
    void throwIfUnsuccessful() {
      if (! bool(*this))
        throw UnsuccessfulStatus(getStatusString(), statusCode);
    }

  private:
    static std::map<int, std::string> HttpStatusCodes;

    int statusCode { -1 };
     HeadersType headers;
    std::string body;
  };

  std::ostream & operator<<(std::ostream & stream, HttpResponse const & response);
}

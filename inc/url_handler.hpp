#pragma once

#include <cstring>
#include <curl/curl.h>
#include <map>
#include <stdexcept>
#include <string>

#include "http_response.hpp"
#include "query.hpp"
#include "requestable.hpp"

namespace caldav {
  /** @brief Deal with HTTP.
   *
   *  This class is a cURL wrapper. cURL uses global initialisation and
   *  cleanup, so instances of %UrlHandler will interfere with user's cURL
   *  instances. When first instance is creating, cURL initialization will be
   *  performed with:
   *  @code curl_global_init(CURL_GLOBAL_DEFAULT); @endcode
   *  When last instance of UrlHandler is destructing, cURL cleanup will be
   *  performed with:
   *  @code curl_global_cleanup(); @endcode
   *  @todo Add proxy support.
   *  @todo Add support for self-signed certs.
   *  @todo setVerbose(bool) with curl_easy_setopt(curl, CURLOPT_VERBOSE, true);
   */
  class UrlHandler: virtual public Requestable {
  public:
    /// Specific exception.
    class Error: public std::runtime_error {
    public:
      explicit Error(std::string const & errorMessage):
      std::runtime_error(errorMessage) { }
    };

    /** @param username Valid username.
     *  @param password Valid password.
     *  @throw UrlHandler::Error on fail.
     */
    UrlHandler(std::string username="", std::string password="");

    virtual ~UrlHandler();

    /// @throw UrlHandler::Error on fail.
    UrlHandler(const UrlHandler &other);

    /// @throw UrlHandler::Error on fail.
    UrlHandler& operator=(const UrlHandler &);

    UrlHandler(UrlHandler &&other);

    UrlHandler& operator=(UrlHandler &&other);

    /** @brief Perform HTTP-request.
     *
     *  @param url Valid URL to requst by.
     *  @param query Configured Query that provides request headers and body.
     *  @return Server response.
     *  @throw UrlHandler::Error Will be thrown when unable to recieve a valid
     *  response.
     */
    HttpResponse request(std::string const & url, Query const & query) override;

    /// Set custom User-Agent HTTP header.
    void setUserAgent(std::string userAgent) {
      curl_easy_setopt(curl, CURLOPT_USERAGENT, userAgent.c_str());
    }

    /** @brief Change user's credentials.
     *  @param username Valid username.
     *  @param password Valid password.
     */
    void setCredentials(std::string username, std::string password);

    /** @brief Use Unix domain socket instead of TCP one.
     *  @param socket Path to Unix socket file.
     *  @throw UrlHandler::Error on fail.
     */
    void setUnixSocket(std::string socket) {
      if (curl_easy_setopt(curl, CURLOPT_UNIX_SOCKET_PATH, socket.c_str()) != CURLE_OK)
        throw UrlHandler::Error("Invalid socket path given: " + socket);
    }
    /// Use TCP socket instead of Unix domain one.
    void unsetUnixSocket() {
      curl_easy_setopt(curl, CURLOPT_UNIX_SOCKET_PATH, nullptr);
    }

    /** @brief Set timeout for request.
     *  @param timeoutSec Seconds for waiting request result. Zero for no timeout.
     */
    void setTimeout(unsigned int timeoutSec) {
      if (curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeoutSec) != CURLE_OK)
        throw UrlHandler::Error("Invalid timeout value: " + std::to_string(timeoutSec));
    }

    /// Wait for request result eternally.
    void unsetTimeout() { curl_easy_setopt(curl, CURLOPT_TIMEOUT, 0); }

    /// Get instances counter value.
    static unsigned int getInstancesCounter() { return counter; }

  private:
    /// cURL handle.
    CURL *curl;

    /// Count instances to manage global initialization of cURL.
    static inline unsigned int counter { 0 };

    static inline std::string const defaultUserAgent { "libcaldavcpp" };

    /// Callback for curl to save response to std::string.
    static size_t writeCallback(
        void *content, size_t size, size_t nmemb, std::string *buffer);

    /// Callback for curl to save headers to std::map.
    static size_t headerCallback(
        char *content, size_t size,
        size_t nitems,
        std::map<std::string, std::string> *buffer);
  };
}

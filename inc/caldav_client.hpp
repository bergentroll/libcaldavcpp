#pragma once

#include <string>
#include <map>

#include "caldav_handler.hpp"
#include "node.hpp"

namespace caldav {
  class CalDavClient {
  public:
    explicit CalDavClient(std::shared_ptr<Requestable> urlHandler, std::string baseUrl);

    // TODO
    void testConnection() const;

    // TODO
    void addItem();

    // TODO
    void sync();

    operator std::string() const;

  private:
    CalDavHandler handler;

    std::string
      /// Server root URL getted from baseUrl.
      rootUrl,
      ///
      calendarHomeSet;

      std::map <std::string, Node> data { };
  };

  /** @brief Parse URL to get root URL.
   *
   * @param url Supposed to be a valid full-qualified URL.
   * @return Protocol + host, e.g. "https://example.com".
   */
  std::string getRootUrl(std::string url);

  /** @brief Split an item path on directory path and name.
   * @param itemUrl Path of item.
   * @return Pair of string, first of pair is directory path, second is name.
   */
  std::pair<std::string, std::string> splitItemUrl(std::string itemUrl);
}

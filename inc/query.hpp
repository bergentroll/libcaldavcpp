#pragma once

#include <string>
#include <vector>

namespace caldav {
  /** Carry data for HTTP request.
   *
   *  @todo Hierarhy is ugly. Refactoring needed.
   */
  struct Query {
    std::string
      /// Request body.
      data,
      /// HTTP method.
      method;
    /// HTTP headers.
    std::vector<std::string> headers;
  };

  /** @brief Request single property.
   *
   *  @todo Condider to add addPropery method for multi-requests.
   */
  struct QueryPropfind: public Query {
    /// @param field Valid xml tag with namespace and propery name.
    QueryPropfind(std::string const & field);
  };

  /// Update CalDAV property.
  struct QueryProppatchDisplayname: public Query {
    /// @param displayname User-friendly name.
    QueryProppatchDisplayname(std::string const & displayname);
  };

  /// Create Collection.
  struct QueryMkCalendar: public Query {
    /// @param displayname User-friendly name.
    QueryMkCalendar(std::string const & displayname);
  };

  const QueryPropfind
    /// Ask for current user's URL.
    queryPropPrincipal { "<d:current-user-principal />" },
    /// Ask for current user's calendar home URL.
    queryPropCalendarHomeSet { "<c:calendar-home-set />" },
    /// Ask for only calendars ctags (that is global for a calendar).
    queryGetCTag { "<cs:getctag />" };

  extern const Query
    /// Ask for calendars metadata.
    queryCalendars,
    /// Ask for content of calendars.
    queryDownload,
    /// Get data.
    queryGet,
    /// Put or change data on server.
    queryPut,
    /// Delete object by URL.
    queryDelete;
}

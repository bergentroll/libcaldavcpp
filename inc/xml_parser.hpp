#pragma once

#include <string>
#include <vector>

#include <pugixml.hpp>

namespace caldav {
  class XmlParser {
  public:
    /**
     * @throw
     */
    explicit XmlParser(std::string const & xml);

    /**
     * @throw std::logic_error
     * @throw std::runtime_error
     */
    std::string get(std::string const & xPath);

    /**
     * @throw std::runtime_error
     */
    std::vector<std::string> getMultiple(std::string const & xPath);

    operator std::string();

  private:
    pugi::xml_document xmlDoc { };
  };
}

# Техническое задание на разработку библиотеки caldavcpp

## Термины

- Библиотека — библиотека caldavcpp, являющаяся объектом данного документа.
- Разработчик — инженер-разработчик Библиотеки.
- Клиент — пользователь библиотеки (программист, либо производный продукт, либо
  пользователь производного продукта).
- Сервер — CalDAV-сервер (RFC4791), доступный для запросов клиента.

## Назначение

Библиотека caldavcpp предназначена для упрощения построения клиентских
приложений, взаимодействующих с Сервером.

## Ограничения

1. Библиотека ограничивается подмножеством вызовов CalDAV.
1. Библиотека не хранит данные — хранение данных остаётся ответственностью
   Пользователя.
1. Библиотека предоставляет итнерфейс только для работы с протоколом CalDAV. Не
   предоставляется интерфес для работы с HTTP, WebDAV и иными протоколами
   (однако части библиотеки могут быть использованы для реализации подобных
   интерфесов).
1. Библиотека следует идеологии «правда на сервере», т. е. конфликт разрешается
   всегда в пользу данных на Сервере.

## Цели

Разработка Библиотеки преследует цели:

1. Сдача Разработчиком квалификационного проекта по курсу C++.
1. Создание Разработчиком простого клиентского графического приложения для
   работы со списками дел (TODO-list) с возможностью синхронизации с
   Сервером.
1. Участие Разработчика в сообществе открытого кода.

## Альтернативы

- [libcaldav](https://sourceforge.net/projects/libcaldav/) — аналогичная
  библиотека на чистом C, находится в состоянии стагнации.
- [Python caldav](https://github.com/python-caldav/caldav) — аналогичная
  библиотека на языке Python. Заявлена полная поддерка стандарта CalDAV.
- [akonadi](https://invent.kde.org/pim/akonadi) — мощный разносторонний
  фреймворк от проекта KDE, поддерживающий CalDAV. Известен своей
  комплексностью и обилием проблем, особенно при обновлениях.
- [evolution-data-server](https://gitlab.gnome.org/GNOME/evolution-data-server)
  — подобный akonadi инструмент от проекта GNOME.

## Функциональные требования

### Интерфейс

Библиотека должна предоставлять интерфес для настройки параметров подключения к
Серверу.

Библиотека должна предоставлять интерфейс для выполнения обращений к Серверу.
Один запрос должен соответствовать одному методу или функции Библиотеки.

### Запросы

Библиотека должны реализовать следующие запросы (в скобках указано название
используемого HTTP-запроса):

- Получение свойств объектов (PROPFIND), как минимум:
  - principal,
  - calendar\_home\_set,
  - ctag.
- Скачивание объекта (GET).
- Загрузка объекта на Сервер (PUT).
- Удаление объекта с сервера (DELETE).
- Создание коллекции (MKCALENDAR).
- Изменение свойства объекта (PROPPATCH).

Опционально библиотека может реализовать иные запросы.

### Соединение

Библиотека должна устанавливать соединение с IPv4-сокетом сервера и совершает
HTTP-запросы. Библиотека может делать несколько запросов в рамках одного
соединения. Желательной является поддержка SOCKS5-прокси.

### Ошибки

Библиотека должна обрабатывать ошибки подключения к Серверу, предоставляя
Клиенту возможность продолжить работу.

Библиотека не должна проверять корректность данных, поступивших от Клиента или
Сервера.

## Нефункциональные требования

Библиотека должна быть написана на «современном C++», соответствовать и быть
совместимой со стандартом ISO/IEC 14882:2017 (С++17).

Библиотека должна иметь возможность собираться при помощи системы сборки CMake
компиляторами GСС и Clang в GNU/Linux-окружении.

Библиотека должна быть документирована при помощи Doxygen.

Должно быть обеспечено регрессионное тестирование Библиотеки.

От библиотеки не требуется асинхронное выполнение и потокобезопасность, однако
соответствующие механизмы могут быть заложены Разработчиком в настоящее время
или в будущем.

## Отказ от ответственности

Библиотека является свободным программным обеспечением. Разработчик не несёт
ответственности за возможный ущерб, возникший при эксплуатации Библиотеки.
Разработчик не несёт ответственности за полноту и корректность Библиотеки.
Разработчик не несёт ответственности за достоверность документации. Разработчик
несёт ответственность только за своих котиков.
